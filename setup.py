#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requires = [
    'setuptools',
    'docutils',
    'jinja2',
    'sphinx',
]

setup_requirements = ['pytest-runner', ]
test_requirements = ['pytest', ]

setup(
    author="Peter Koppatz",
    author_email='peter@koppatz.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Mathematics',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.12',
    ],
    description="org2sphinx",
    install_requires=requires,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    long_description_content_type='text/x-rst',
    include_package_data=True,
    name='org2sphinx',
    keywords='org2sphinx',
    packages=['org2sphinx'],
    package_dir={'': 'src'},
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='git@gitlab.com:opendata-apps/org2sphinx.git',
    download_url='https://gitlab.com/opendata-apps/org2sphinx.git',
    version='0.0.2',
    zip_safe=False,
)
