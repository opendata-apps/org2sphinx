orgsphinx
=========

Getting Started
---------------

- Change directory into your newly created project.

  .. code::
  
     cd /go/to/your/project/folder

- Create a Python virtual environment.

  .. code::

     python3 -m venv .org2sphinx

- Upgrade packaging tools.

  .. code::

    env/bin/pip install --upgrade pip setuptools
    env/bin/pip install --upgrade pip pip

- Install orgsphinx

  .. code::

     source .org2sphinx/bin/activate
     git clone https://gitlab.com/opendata-apps/org2sphinx.git
     cd org2sphinx
     pip install -e ".[testing]"

    
- Setup an Sphinx-Projekt

  You can find the documentation at:
  
  https://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html

- Konfiguration for Org-Files

  In conf.py add the following lines:
  
  .. code::

     import org2sphinx

     def setup(app):
         app.add_source_suffix('.org', 'org2sphinx')
         app.add_source_parser(org2sphinx.OrgParser)

     source_parsers = {'.org': 'OrgParser'}

     source_suffix = {
         '.rst': 'restructuredtext',
         '.txt': 'restructuredtext',
         '.org': 'org2sphinx',
     }
