.. highlight:: shell

============
Contributing
============

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

Types of Contributions
----------------------

Report Bugs
~~~~~~~~~~~

Report bugs at https://gitlab.com/orgsphinx/issues.

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

Fix Bugs
~~~~~~~~

Look through the GitLab issues for bugs. Anything tagged with "bug" and "help
wanted" is open to whoever wants to implement it.

Implement Features
~~~~~~~~~~~~~~~~~~

Look through the GitHub issues for features. Anything tagged with "enhancement"
and "help wanted" is open to whoever wants to implement it.

Write Documentation
~~~~~~~~~~~~~~~~~~~

gitlab.orgsphinx could always use more documentation, whether as part of the
official orgsphinx.koppatz.com, in docstrings, or even on the web in blog posts,
articles, and such.

Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an issue at
https://gitlab.com/opendata-apps/org2sphinx/-/issues

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

Get Started!
------------

Ready to contribute? Here's how to set up `org2sphinx` for local development.

1. Fork the `orgsphinx` repo on GitLab.
2. Clone your fork locally::



3. Install your local copy into a virtualenv.
   Assuming you have virtualenvwrapper installed, this is how you set up
   your fork for local development::

     mkdir myshinxprojekt
     python3 -m venv .mysphinxproject
     source .mysphinxprojekt/bin/activate
     git clone git@gitlab.com:org2sphinx.git
     cd org2sphinx
     pip install -e ".[testing]"
     
   
4. Now you are ready to manage your files::

     cd mysphinxproject
     pip install sphinx
     pip install furo
     sphinx-quickstart .

