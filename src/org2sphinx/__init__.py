"""Org2Sphinx parser"""

import pathlib
import subprocess
from subprocess import PIPE
from sphinx import parsers
from .orgparser import RSTParser

__docformat__ = 'org2sphinx'

__all__ = ['OrgParser']


class OrgParser(parsers.Parser):
    """Docutils parser for org files"""

    supported = ('org', 'org2sphinx')
    translate_section_name = None
    level = 0

    def get_transforms(self):
        """ call transform """
        transforms = super().get_transforms()
        return transforms

    def parse(self, inputstring, document):
        """Parse text and generate a document tree."""

        self.document = document
        self.current_node = document
        doc = str(document)
        doc = doc.split('"')[1]
        filepath = pathlib.Path(doc[:-4] + ".rst")
        filename = pathlib.Path(doc).name
        command = [
            'emacsclient', '-q',
            '--eval', f'(progn (find-file "{doc}") (org-rst-export-to-rst))'
        ]
        #    '--load', '/home/ram/disk/init.el',
        

        p = subprocess.Popen(command, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate(b"data is passed to subprocess' stdin")

        command2 = [
            'cat', f'{filepath}',
        ]

        proc = subprocess.Popen(command2, stdout=subprocess.PIPE)
        inputstring = proc.stdout.read().decode('utf-8')

        proc = subprocess.Popen(command2, stdout=subprocess.PIPE)
        inputstring = proc.stdout.read().decode('utf-8')

        command3 = [
            'emacsclient', '-q',
            '--eval', f'(progn (kill-buffer "{filename}"))'
        ]
        p = subprocess.Popen(command3, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate(b"data is passed to subprocess' stdin")

        
        self.document.settings.tab_width = 4
        parser = RSTParser()
        ast = parser.parse(inputstring, document)
        try:
            tempfile = pathlib.Path(filepath)
            tempfile.unlink()
        except FileNotFoundError:
            print('Something went wrong')


def setup(app):
    app.add_source_parser(OrgParser)

    return {
        'version': 'builtin',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
